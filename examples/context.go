package main

import (
	"errors"
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	base := jl.NewLogger(os.Stdout, jl.Debug)
	// make a sub-logger with some static context fields
	log := base.With().E(errors.New("something bad happened")).Ctx()
	// use the sub-logger
	log.Error("oh no!").End()
	log.Fatal("there is nothing we can do now!").End()
}
