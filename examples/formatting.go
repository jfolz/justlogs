package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// a formatted message
	log.Debug("%s %d %v", "text", 234, []byte{1, 2, 3, 4}).End()
	// a formatted string
	log.Info("formatted strings").S("formatted", "%f %s", 1.213, "string").End()
	// formatted string in an array
	log.Info("formatted strings").A("formatarray").
		S("%s:%f", "a", 1.5).S("%s:%f", "b", 9.32).End().End()
}
