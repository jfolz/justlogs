package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// use unix timestamp
	log.TimestampFunc = jl.UnixSecs
	log.Debug("with unix timestamp").End()
	// turn on human-readable timestamp
	log.TimestampFunc = jl.UTCMicros
	log.Debug("with human-readable timestamp").End()
	// turn on caller
	log.Caller = 1
	log.Debug("now with caller").End()
}
