package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// just a message
	log.Debug("just some text").End()
	// add fields to your logs
	log.Info("hello logs").
		S("greeting", "yes").
		B("success", true).
		I("number", 42).
		H("hex", []byte{1, 2, 3, 4}).
		End()
}
