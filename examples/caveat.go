package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// repeated field name
	log.Info("the message").I("number", 1).I("number", 2).I("number", 3).End()
	// use a "reserved" field name
	log.Info("the message").S("msg", "more message?").End()
}
