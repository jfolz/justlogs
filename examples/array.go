package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// start a new array
	array := log.Info("logging some arrays").A("myarray")
	// add some ints to it
	for _, i := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {
		array.I(i)
	}
	// and a string for some reason
	array.S("I am also a number")
	// finish the array and output the line
	array.End().F32("notarray", 0.1).End()
}
