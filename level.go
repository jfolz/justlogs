package justlogs

import (
	"errors"
	"strings"
)

var (
	// Default levels
	Trace = NewLevel(36, "trace")
	Debug = NewLevel(72, "debug")
	Info  = NewLevel(108, "info")
	Warn  = NewLevel(144, "warn")
	Error = NewLevel(180, "error")
	Fatal = NewLevel(216, "fatal")
	// Map of name to Level objects.
	NameToLevel = map[string]Level{
		"trc":     Trace,
		"trace":   Trace,
		"dbg":     Debug,
		"debug":   Debug,
		"inf":     Info,
		"info":    Info,
		"warn":    Warn,
		"warning": Warn,
		"err":     Error,
		"erro":    Error,
		"error":   Error,
		"fat":     Fatal,
		"fata":    Fatal,
		"fatal":   Fatal,
	}
	// Returned by ParseLevel when an invalid name is given.
	ErrParseLevel = errors.New("invalid level name")
)

type Level struct {
	Repr     []byte
	Severity uint8
}

func NewLevel(severity uint8, name string) Level {
	buf := make([]byte, 0, 6+len(name))
	buf = AppendStringUnsafe(AppendKey(buf, "level"), name)
	return Level{buf, severity}
}

// Returns name of the level.
func (l *Level) Name() string {
	return string(l.Repr[6:])
}

// Get one of the pre-defined levels by name.
func ParseLevel(name string) (Level, error) {
	name = strings.ToLower(strings.Trim(name, " "))
	level, ok := NameToLevel[name]
	if !ok {
		return Level{}, ErrParseLevel
	}
	return level, nil
}
