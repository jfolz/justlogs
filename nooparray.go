package justlogs

import (
	"time"
)

// Placeholder array that does nothing.
// Used by NoopLine instead of RealArray.
// See Array interface for documentation of its methods.
type NoopArray struct {
	Line Line
}

func (a *NoopArray) B(bool) Array                    { return a }
func (a *NoopArray) C64(complex64) Array             { return a }
func (a *NoopArray) C128(complex128) Array           { return a }
func (a *NoopArray) D(time.Duration) Array           { return a }
func (a *NoopArray) E(error) Array                   { return a }
func (a *NoopArray) F32(float32) Array               { return a }
func (a *NoopArray) F64(float64) Array               { return a }
func (a *NoopArray) H([]byte) Array                  { return a }
func (a *NoopArray) I(int) Array                     { return a }
func (a *NoopArray) I8(int8) Array                   { return a }
func (a *NoopArray) I16(int16) Array                 { return a }
func (a *NoopArray) I32(int32) Array                 { return a }
func (a *NoopArray) I64(int64) Array                 { return a }
func (a *NoopArray) M(Marshaller, interface{}) Array { return a }
func (a *NoopArray) S(string, ...interface{}) Array  { return a }
func (a *NoopArray) T(time.Time, string) Array       { return a }
func (a *NoopArray) Ts(time.Time) Array              { return a }
func (a *NoopArray) Tms(time.Time) Array             { return a }
func (a *NoopArray) Tmu(time.Time) Array             { return a }
func (a *NoopArray) Tn(time.Time) Array              { return a }
func (a *NoopArray) U(uint) Array                    { return a }
func (a *NoopArray) U8(uint8) Array                  { return a }
func (a *NoopArray) U16(uint16) Array                { return a }
func (a *NoopArray) U32(uint32) Array                { return a }
func (a *NoopArray) U64(uint64) Array                { return a }
func (a *NoopArray) V(interface{}) Array             { return a }
func (a *NoopArray) Zs(time.Time) Array              { return a }
func (a *NoopArray) Zms(time.Time) Array             { return a }
func (a *NoopArray) Zmu(time.Time) Array             { return a }
func (a *NoopArray) Zn(time.Time) Array              { return a }

func (a *NoopArray) End() Line {
	if a == nil {
		var nothing *NoopLine
		return nothing
	}
	return a.Line
}
