package justlogs

import (
	"time"
)

type Array interface {
	// Add a Boolean.
	B(bool) Array
	// Add a complex64.
	C64(complex64) Array
	// Add a complex64.
	C128(complex128) Array
	// Add a duration.
	D(time.Duration) Array
	// Add an error.
	E(error) Array
	// Finish the Array.
	End() Line
	// Add a float32.
	F32(float32) Array
	// Add a float64.
	F64(float64) Array
	// Add some bytes in hex representation.
	H([]byte) Array
	// Add an int.
	I(int) Array
	// Add an int8.
	I8(int8) Array
	// Add an int16.
	I16(int16) Array
	// Add an int32.
	I32(int32) Array
	// Add an int64.
	I64(int64) Array
	// Add a value using a marshalling function
	M(Marshaller, interface{}) Array
	// Add a string.
	// Applies formatting with `fmt.Sprintf(string, args...)`
	// if additional args are given.
	// Formatting should be avoided if speed is important.
	S(string, ...interface{}) Array
	// Add Time using the given format
	T(time.Time, string) Array
	// Add Time with timezone and second precision.
	Ts(time.Time) Array
	// Add Time with timezone and millisecond precision.
	Tms(time.Time) Array
	// Add Time with timezone and microsecond precision.
	Tmu(time.Time) Array
	// Add Time with timezone and nanosecond precision.
	Tn(time.Time) Array
	// Add a uint.
	U(uint) Array
	// Add a uint8.
	U8(uint8) Array
	// Add a uint16.
	U16(uint16) Array
	// Add a uint32.
	U32(uint32) Array
	// Add a uint64.
	U64(uint64) Array
	// Add an any value as represented by fmt.Sprint.
	V(interface{}) Array
	// Add Time in UTC with second precision.
	Zs(time.Time) Array
	// Add Time in UTC with millisecond precision.
	Zms(time.Time) Array
	// Add Time in UTC with microsecond precision.
	Zmu(time.Time) Array
	// Add Time in UTC with nanosecond precision.
	Zn(time.Time) Array
}
