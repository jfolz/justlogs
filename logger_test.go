package justlogs

import (
	"io"
	"os"
	"testing"
	"time"
)

const (
	msg = "some logging message"
)

var (
	jl = NewLogger(io.Discard, Info)
)

func BenchmarkDisabled(b *testing.B) {
	logger := jl.With().Ctx()
	logger.Disabled = true
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jl.Debug(msg).End()
	}
}

func BenchmarkSkipped(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jl.Debug(msg).End()
	}
}

func BenchmarkSkippedComplex(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jl.Debug(msg).S("morestuff", "yes").S("good", "no").I("number", 2345).End()
	}
}

func BenchmarkMessage(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jl.Info(msg).End()
	}
}

func BenchmarkTimestamp(b *testing.B) {
	logger := jl.With().Ctx()
	logger.TimestampFunc = UTCSecs
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		logger.Info(msg).End()
	}
}

func BenchmarkFields(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jl.Info(msg).S("morestuff", "yes").S("good", "no").I("number", 2345).End()
	}
}

func BenchmarkContext(b *testing.B) {
	logger := jl.With().I("something", 2).S("anything", ":)").Ctx()
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		logger.Info(msg).End()
	}
}

func BenchmarkArray(b *testing.B) {
	ints := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		array := jl.Info(msg).A("array")
		for _, i := range ints {
			array.I(i)
		}
		array.End().End()
	}
}

func BenchmarkAppendComplex(b *testing.B) {
	buf := make([]byte, 0, 10000000)
	c := complex(1.0235345, -2.45)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf = AppendComplex128(buf, c)
		buf = buf[:0]
	}
}

func BenchmarkAppendFloat(b *testing.B) {
	buf := make([]byte, 0, 10000000)
	f := 12431.2134521
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf = AppendFloat64(buf, f)
		buf = buf[:0]
	}
}

func BenchmarkAppendTime(b *testing.B) {
	buf := make([]byte, 0, 10000000)
	now := time.Now()
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf = AppendTimeMillis(buf, now)
		buf = buf[:0]
	}
}

func TestTest(t *testing.T) {
	log := NewLogger(os.Stdout, Debug)
	log.Error("%s %d", "hello", 645).End()
	log.Info("test").C64("complex64", complex(float32(1.0235345), float32(2.45))).End()
	log.Info("test").C128("complex128", complex(1.0235345, -2.45)).End()
	log.TimestampFunc = TimezoneMillis
	log.Info("test").End()
	log.TimestampFunc = UTCMillis
	log.Info("test").End()
	log.TimestampFunc = UTCMicros
	log.Info("test").End()
	log.TimestampFunc = UTCNanos
	log.Info("test").End()
	log.Info(msg).Zmu("now", time.Now()).Tmu("now", time.Now()).A("array").S("str").I(2345).S("there's a space").End().End()
	log.Info(msg).F32("float", 0.1).End()
	log.Error("%s %d", "hello", 645).E(io.EOF).End()
	log.Error("%s %d", "hello", 645).E(nil).End()
	log.Error("%s %d", "hello", 645).A("array").S("Hi!").E(nil).End().End()
}
