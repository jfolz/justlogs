package justlogs

import (
	"time"
)

type NoopLine struct {
	Logger *Logger
	Array  *NoopArray
}

func NewNoopLine(l *Logger) *NoopLine {
	line := &NoopLine{Logger: l}
	line.Array = &NoopArray{Line: line}
	return line
}

func (l *NoopLine) A(string) Array                         { return l.Array }
func (l *NoopLine) B(string, bool) Line                    { return l }
func (l *NoopLine) C64(string, complex64) Line             { return l }
func (l *NoopLine) C128(string, complex128) Line           { return l }
func (l *NoopLine) Ctx() *Logger                           { return l.Logger }
func (l *NoopLine) D(string, time.Duration) Line           { return l }
func (l *NoopLine) E(error) Line                           { return l }
func (l *NoopLine) End() error                             { return nil }
func (l *NoopLine) F32(string, float32) Line               { return l }
func (l *NoopLine) F64(string, float64) Line               { return l }
func (l *NoopLine) H(string, []byte) Line                  { return l }
func (l *NoopLine) I(string, int) Line                     { return l }
func (l *NoopLine) I8(string, int8) Line                   { return l }
func (l *NoopLine) I16(string, int16) Line                 { return l }
func (l *NoopLine) I32(string, int32) Line                 { return l }
func (l *NoopLine) I64(string, int64) Line                 { return l }
func (l *NoopLine) M(string, Marshaller, interface{}) Line { return l }
func (l *NoopLine) S(string, string, ...interface{}) Line  { return l }
func (l *NoopLine) T(string, time.Time, string) Line       { return l }
func (l *NoopLine) Ts(string, time.Time) Line              { return l }
func (l *NoopLine) Tms(string, time.Time) Line             { return l }
func (l *NoopLine) Tmu(string, time.Time) Line             { return l }
func (l *NoopLine) Tn(string, time.Time) Line              { return l }
func (l *NoopLine) U(string, uint) Line                    { return l }
func (l *NoopLine) U8(string, uint8) Line                  { return l }
func (l *NoopLine) U16(string, uint16) Line                { return l }
func (l *NoopLine) U32(string, uint32) Line                { return l }
func (l *NoopLine) U64(string, uint64) Line                { return l }
func (l *NoopLine) V(string, interface{}) Line             { return l }
func (l *NoopLine) Zs(string, time.Time) Line              { return l }
func (l *NoopLine) Zms(string, time.Time) Line             { return l }
func (l *NoopLine) Zmu(string, time.Time) Line             { return l }
func (l *NoopLine) Zn(string, time.Time) Line              { return l }
