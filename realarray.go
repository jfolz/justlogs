package justlogs

import (
	"fmt"
	"time"
)

// Array implementation that actually does logging.
// Returned by RealLine.A().
// See Array interface for documentation of its methods.
type RealArray struct {
	Line      *RealLine
	NeedComma bool
}

func (a *RealArray) B(val bool) Array {
	a.Line.Buf = AppendBool(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) C64(val complex64) Array {
	a.Line.Buf = AppendComplex64(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) C128(val complex128) Array {
	a.Line.Buf = AppendComplex128(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Comma(buf []byte) []byte {
	if a.NeedComma {
		return append(buf, ',')
	}
	a.NeedComma = true
	return buf
}

func (a *RealArray) D(val time.Duration) Array {
	a.Line.Buf = AppendStringUnsafe(a.Comma(a.Line.Buf), val.String())
	return a
}

func (a *RealArray) E(val error) Array {
	a.Line.Buf = a.Comma(a.Line.Buf)
	if val != nil {
		a.Line.Buf = AppendStringSafe(a.Line.Buf, val.Error())
	} else {
		a.Line.Buf = AppendStringUnsafe(a.Line.Buf, "nil")
	}
	return a
}

func (a *RealArray) End() Line {
	a.NeedComma = false
	return a.Line
}

func (a *RealArray) F32(val float32) Array {
	a.Line.Buf = AppendFloat32(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) F64(val float64) Array {
	a.Line.Buf = AppendFloat64(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) H(val []byte) Array {
	a.Line.Buf = AppendHex(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) I(val int) Array {
	return a.I64(int64(val))
}

func (a *RealArray) I8(val int8) Array {
	return a.I64(int64(val))
}

func (a *RealArray) I16(val int16) Array {
	return a.I64(int64(val))
}

func (a *RealArray) I32(val int32) Array {
	return a.I64(int64(val))
}

func (a *RealArray) I64(val int64) Array {
	a.Line.Buf = AppendInt(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) M(f Marshaller, obj interface{}) Array {
	a.Line.Buf = f(a.Line.Buf, obj)
	return a
}

func (a *RealArray) S(val string, args ...interface{}) Array {
	if len(args) > 0 {
		val = fmt.Sprintf(val, args...)
	}
	a.Line.Buf = AppendStringSafe(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) U(val uint) Array {
	return a.U64(uint64(val))
}

func (a *RealArray) U8(val uint8) Array {
	return a.U64(uint64(val))
}

func (a *RealArray) U16(val uint16) Array {
	return a.U64(uint64(val))
}

func (a *RealArray) U32(val uint32) Array {
	return a.U64(uint64(val))
}

func (a *RealArray) U64(val uint64) Array {
	a.Line.Buf = AppendUint(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) V(val interface{}) Array {
	a.Line.Buf = AppendStringSafe(a.Comma(a.Line.Buf), fmt.Sprint(val))
	return a
}

func (a *RealArray) T(val time.Time, format string) Array {
	a.Line.Buf = AppendTime(a.Comma(a.Line.Buf), val, format)
	return a
}

func (a *RealArray) Ts(val time.Time) Array {
	a.Line.Buf = AppendTimeSecs(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Tms(val time.Time) Array {
	a.Line.Buf = AppendTimeMillis(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Tmu(val time.Time) Array {
	a.Line.Buf = AppendTimeMicros(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Tn(val time.Time) Array {
	a.Line.Buf = AppendTimeNanos(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Zs(val time.Time) Array {
	a.Line.Buf = AppendTimeSecsUTC(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Zms(val time.Time) Array {
	a.Line.Buf = AppendTimeMillisUTC(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Zmu(val time.Time) Array {
	a.Line.Buf = AppendTimeMicrosUTC(a.Comma(a.Line.Buf), val)
	return a
}

func (a *RealArray) Zn(val time.Time) Array {
	a.Line.Buf = AppendTimeNanosUTC(a.Comma(a.Line.Buf), val)
	return a
}
