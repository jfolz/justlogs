package justlogs

import (
	"fmt"
	"io"
	"os"
	"sync"
	"time"
)

var (
	// Initial capacity of line buffers.
	// Big enough to avoid reallocations in all but the most extreme cases.
	// This should not be a concern unless you have literally thousands of
	// threads, yet somehow still concerned with a couple MBs of memory.
	InitialLineBufferSize = 4096
	// If a line buffer exceeds this size it is not put back into the
	// pool for reuse but discarded instead.
	MaxLineBufferSize = 16384
	// Pool to reuse allocated lines.
	RealLinePool = &sync.Pool{
		New: func() interface{} {
			line := &RealLine{Buf: make([]byte, 0, InitialLineBufferSize)}
			line.Array = &RealArray{Line: line}
			return line
		},
	}
)

// Get a RealLine for use from the pool.
func GetRealLine(logger *Logger, writer io.Writer, exit bool) *RealLine {
	line, _ := RealLinePool.Get().(*RealLine)
	line.Buf = line.Buf[:0]
	line.Logger = logger
	line.Writer = writer
	line.exit = exit
	// ensure correct state even if previous array was not closed
	line.Array.NeedComma = false
	return line
}

// Release a RealLine so it may be put back into the pool.
// Lines will be discarded if their buffer exceeds MaxLineBufferSize.
func ReleaseRealLine(line *RealLine) {
	// Only keep lines with buffers <= 16KiB, because https://golang.org/issue/23199
	if cap(line.Buf) <= MaxLineBufferSize {
		// set these to nil so we don't hold references forever
		line.Logger = nil
		line.Writer = nil
		RealLinePool.Put(line)
	}
}

// Line implementation that actually does logging.
// Returned by a Logger when severity is as high or higher than the set level.
// See Line interface for documentation of its methods.
type RealLine struct {
	Buf    []byte
	Logger *Logger
	Writer io.Writer
	Array  *RealArray
	exit   bool
}

func (l *RealLine) A(key string) Array {
	l.Buf = AppendSpaceKey(l.Buf, key)
	return l.Array
}

func (l *RealLine) B(key string, val bool) Line {
	l.Buf = AppendBool(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) C64(key string, val complex64) Line {
	l.Buf = AppendComplex64(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) C128(key string, val complex128) Line {
	l.Buf = AppendComplex128(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Ctx() *Logger {
	// make a copy of the logger and add the buffer to its context
	logger := *l.Logger
	logger.Context = append(l.Logger.Context, l.Buf...)
	// return this line to the pool
	ReleaseRealLine(l)
	return &logger
}

func (l *RealLine) D(key string, val time.Duration) Line {
	l.Buf = AppendDuration(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) E(val error) Line {
	l.Buf = AppendSpaceKey(l.Buf, l.Logger.ErrorField)
	if val != nil {
		l.Buf = AppendStringSafe(l.Buf, val.Error())
	} else {
		l.Buf = AppendStringUnsafe(l.Buf, "nil")
	}
	return l
}

func (l *RealLine) End() error {
	// add newline and write out the buffer content
	buf := append(l.Buf, '\n')
	_, err := l.Writer.Write(buf)
	// return this line to the pool
	ReleaseRealLine(l)
	// severity >= fatal, so exit
	if l.exit {
		os.Exit(1)
	}
	return err
}

func (l *RealLine) F32(key string, val float32) Line {
	l.Buf = AppendFloat32(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) F64(key string, val float64) Line {
	l.Buf = AppendFloat64(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) H(key string, val []byte) Line {
	l.Buf = AppendHex(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) I(key string, val int) Line {
	return l.I64(key, int64(val))
}

func (l *RealLine) I8(key string, val int8) Line {
	return l.I64(key, int64(val))
}

func (l *RealLine) I16(key string, val int16) Line {
	return l.I64(key, int64(val))
}

func (l *RealLine) I32(key string, val int32) Line {
	return l.I64(key, int64(val))
}

func (l *RealLine) I64(key string, val int64) Line {
	l.Buf = AppendInt(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) M(key string, f Marshaller, obj interface{}) Line {
	l.Buf = f(AppendSpaceKey(l.Buf, key), obj)
	return l
}

func (l *RealLine) S(key, val string, args ...interface{}) Line {
	if len(args) > 0 {
		val = fmt.Sprintf(val, args...)
	}
	l.Buf = AppendStringSafe(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) T(key string, val time.Time, format string) Line {
	l.Buf = AppendTime(AppendSpaceKey(l.Buf, key), val, format)
	return l
}

func (l *RealLine) Ts(key string, val time.Time) Line {
	l.Buf = AppendTimeSecs(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Tms(key string, val time.Time) Line {
	l.Buf = AppendTimeMillis(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Tmu(key string, val time.Time) Line {
	l.Buf = AppendTimeMicros(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Tn(key string, val time.Time) Line {
	l.Buf = AppendTimeNanos(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) U(key string, val uint) Line {
	return l.U64(key, uint64(val))
}

func (l *RealLine) U8(key string, val uint8) Line {
	return l.U64(key, uint64(val))
}

func (l *RealLine) U16(key string, val uint16) Line {
	return l.U64(key, uint64(val))
}

func (l *RealLine) U32(key string, val uint32) Line {
	return l.U64(key, uint64(val))
}

func (l *RealLine) U64(key string, val uint64) Line {
	l.Buf = AppendUint(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) V(key string, val interface{}) Line {
	l.Buf = AppendStringSafe(AppendSpaceKey(l.Buf, key), fmt.Sprint(val))
	return l
}

func (l *RealLine) Zs(key string, val time.Time) Line {
	l.Buf = AppendTimeSecsUTC(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Zms(key string, val time.Time) Line {
	l.Buf = AppendTimeMillisUTC(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Zmu(key string, val time.Time) Line {
	l.Buf = AppendTimeMicrosUTC(AppendSpaceKey(l.Buf, key), val)
	return l
}

func (l *RealLine) Zn(key string, val time.Time) Line {
	l.Buf = AppendTimeNanosUTC(AppendSpaceKey(l.Buf, key), val)
	return l
}
