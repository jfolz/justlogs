package justlogs

import (
	"time"
)

type Marshaller = func([]byte, interface{}) []byte

type Line interface {
	// Start an array.
	A(string) Array
	// Add a Boolean.
	B(string, bool) Line
	// Add a complex64.
	C64(string, complex64) Line
	// Add a complex64.
	C128(string, complex128) Line
	// Create a copy of the logger with added context.
	Ctx() *Logger
	// Add a duration.
	D(string, time.Duration) Line
	// Add an error.
	E(error) Line
	// Output the line.
	End() error
	// Add a float32.
	F32(string, float32) Line
	// Add a float64.
	F64(string, float64) Line
	// Add some bytes in hex representation.
	H(string, []byte) Line
	// Add an int.
	I(string, int) Line
	// Add an int8.
	I8(string, int8) Line
	// Add an int16.
	I16(string, int16) Line
	// Add an int32.
	I32(string, int32) Line
	// Add an int64.
	I64(string, int64) Line
	// Add a value using a marshalling function
	M(string, Marshaller, interface{}) Line
	// Add a string.
	// Applies formatting with `fmt.Sprintf(string, args...)`
	// if additional args are given.
	// Formatting should be avoided if speed is important.
	S(string, string, ...interface{}) Line
	// Add Time using the given format
	T(string, time.Time, string) Line
	// Add Time with timezone and second precision.
	Ts(string, time.Time) Line
	// Add Time with timezone and millisecond precision.
	Tms(string, time.Time) Line
	// Add Time with timezone and microsecond precision.
	Tmu(string, time.Time) Line
	// Add Time with timezone and nanosecond precision.
	Tn(string, time.Time) Line
	// Add a uint.
	U(string, uint) Line
	// Add a uint8.
	U8(string, uint8) Line
	// Add a uint16.
	U16(string, uint16) Line
	// Add a uint32.
	U32(string, uint32) Line
	// Add a uint64.
	U64(string, uint64) Line
	// Add an any value as represented by fmt.Sprint.
	V(string, interface{}) Line
	// Add Time in UTC with second precision.
	Zs(string, time.Time) Line
	// Add Time in UTC with millisecond precision.
	Zms(string, time.Time) Line
	// Add Time in UTC with microsecond precision.
	Zmu(string, time.Time) Line
	// Add Time in UTC with nanosecond precision.
	Zn(string, time.Time) Line
}
