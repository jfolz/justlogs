package justlogs

import (
	"time"
)

var (
	indexToDigits = [100][2]byte{
		{'0', '0'}, {'0', '1'}, {'0', '2'}, {'0', '3'}, {'0', '4'},
		{'0', '5'}, {'0', '6'}, {'0', '7'}, {'0', '8'}, {'0', '9'},
		{'1', '0'}, {'1', '1'}, {'1', '2'}, {'1', '3'}, {'1', '4'},
		{'1', '5'}, {'1', '6'}, {'1', '7'}, {'1', '8'}, {'1', '9'},
		{'2', '0'}, {'2', '1'}, {'2', '2'}, {'2', '3'}, {'2', '4'},
		{'2', '5'}, {'2', '6'}, {'2', '7'}, {'2', '8'}, {'2', '9'},
		{'3', '0'}, {'3', '1'}, {'3', '2'}, {'3', '3'}, {'3', '4'},
		{'3', '5'}, {'3', '6'}, {'3', '7'}, {'3', '8'}, {'3', '9'},
		{'4', '0'}, {'4', '1'}, {'4', '2'}, {'4', '3'}, {'4', '4'},
		{'4', '5'}, {'4', '6'}, {'4', '7'}, {'4', '8'}, {'4', '9'},
		{'5', '0'}, {'5', '1'}, {'5', '2'}, {'5', '3'}, {'5', '4'},
		{'5', '5'}, {'5', '6'}, {'5', '7'}, {'5', '8'}, {'5', '9'},
		{'6', '0'}, {'6', '1'}, {'6', '2'}, {'6', '3'}, {'6', '4'},
		{'6', '5'}, {'6', '6'}, {'6', '7'}, {'6', '8'}, {'6', '9'},
		{'7', '0'}, {'7', '1'}, {'7', '2'}, {'7', '3'}, {'7', '4'},
		{'7', '5'}, {'7', '6'}, {'7', '7'}, {'7', '8'}, {'7', '9'},
		{'8', '0'}, {'8', '1'}, {'8', '2'}, {'8', '3'}, {'8', '4'},
		{'8', '5'}, {'8', '6'}, {'8', '7'}, {'8', '8'}, {'8', '9'},
		{'9', '0'}, {'9', '1'}, {'9', '2'}, {'9', '3'}, {'9', '4'},
		{'9', '5'}, {'9', '6'}, {'9', '7'}, {'9', '8'}, {'9', '9'},
	}
)

func appendDigits(buf []byte, index int) []byte {
	digits := indexToDigits[index]
	return append(buf, digits[0], digits[1])
}

// Format a Time with the given format,
// append the result to the given buffer,
// and return the new buffer.
// Alias for Time.AppendFormat.
func AppendTime(buf []byte, val time.Time, format string) []byte {
	return val.AppendFormat(buf, format)
}

// Format a Time with format "2006-01-02T15:04:05",
// append the result to the given buffer,
// and return the new buffer.
// Musch faster than AppendTime.
func AppendTimeFast(buf []byte, val time.Time) []byte {
	y, m, d := val.Date()
	h, mi, s := val.Clock()
	f := y / 100
	return appendDigits(append(appendDigits(append(appendDigits(append(
		appendDigits(append(appendDigits(append(appendDigits(appendDigits(buf,
			f), y-100*f), '-'), int(m)), '-'), d), 'T'), h), ':'), mi), ':'), s)
}

// Append just the "." and fractional second of a Time to the given buffer,
// and return the new buffer.
// Equivalent to AppendTime with format ".999999999",
// where prec determines the number of 9s.
func AppendSecFracFast(buf []byte, val time.Time, prec int) []byte {
	buf = AppendInt(append(buf, '.'), int64(val.Nanosecond()))
	n := len(buf)
	return buf[:n-9+prec]
}

// Append just the timezone offset to the given buffer
// and return the new buffer.
// Equivalent to AppendTime with format "-0700".
func AppendTzOffset(buf []byte, val time.Time) []byte {
	_, offset := val.Zone()
	var pm byte
	if offset < 0 {
		pm = '-'
		offset = -offset
	} else {
		pm = '+'
	}
	offset /= 60
	h := offset / 60
	m := offset - h*60
	return appendDigits(appendDigits(append(buf, pm), h), m)
}

// Append the given Time with timezone and second precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05-0700".
func AppendTimeSecs(buf []byte, val time.Time) []byte {
	return AppendTzOffset(AppendTimeFast(buf, val), val)
}

// Append the given Time in UTC with second precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05Z".
func AppendTimeSecsUTC(buf []byte, val time.Time) []byte {
	return append(AppendTimeFast(buf, val.UTC()), 'Z')
}

// Append the given Time with timezone and millisecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999-0700".
func AppendTimeMillis(buf []byte, val time.Time) []byte {
	return AppendTzOffset(AppendSecFracFast(AppendTimeFast(buf, val), val, 3), val)
}

// Append the given Time in UTC with millisecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999Z".
func AppendTimeMillisUTC(buf []byte, val time.Time) []byte {
	val = val.UTC()
	return append(AppendSecFracFast(AppendTimeFast(buf, val), val, 3), 'Z')
}

// Append the given Time with timezone and microsecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999-0700".
func AppendTimeMicros(buf []byte, val time.Time) []byte {
	return AppendTzOffset(AppendSecFracFast(AppendTimeFast(buf, val), val, 6), val)
}

// Append the given Time in UTC with microsecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999Z".
func AppendTimeMicrosUTC(buf []byte, val time.Time) []byte {
	val = val.UTC()
	return append(AppendSecFracFast(AppendTimeFast(buf, val), val, 6), 'Z')
}

// Append the given Time with timezone and nanosecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999999-0700".
func AppendTimeNanos(buf []byte, val time.Time) []byte {
	return AppendTzOffset(AppendSecFracFast(AppendTimeFast(buf, val), val, 6), val)
}

// Append the given Time in UTC with nanosecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999999Z".
func AppendTimeNanosUTC(buf []byte, val time.Time) []byte {
	val = val.UTC()
	return append(AppendSecFracFast(AppendTimeFast(buf, val), val, 9), 'Z')
}

// Human-readable timestamp with timezone and second precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05-0700".
func TimezoneSecs(buf []byte) []byte {
	return AppendTimeSecs(buf, time.Now())
}

// Human-readable timestamp with timezone and millisecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999-0700".
func TimezoneMillis(buf []byte) []byte {
	return AppendTimeMillis(buf, time.Now())
}

// Human-readable timestamp with timezone and microsecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999-0700".
func TimezoneMicros(buf []byte) []byte {
	return AppendTimeMicros(buf, time.Now())
}

// Human-readable timestamp with timezone and nanosecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999999-0700".
func TimezoneNanos(buf []byte) []byte {
	return AppendTimeNanos(buf, time.Now())
}

// Human-readable timestamp in UTC with second precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05Z".
func UTCSecs(buf []byte) []byte {
	return append(AppendTimeFast(buf, time.Now().UTC()), 'Z')
}

// Human-readable timestamp in UTC with millisecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999Z".
func UTCMillis(buf []byte) []byte {
	return AppendTimeMillisUTC(buf, time.Now())
}

// Human-readable timestamp in UTC with microsecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999Z".
func UTCMicros(buf []byte) []byte {
	return AppendTimeMicrosUTC(buf, time.Now())
}

// Human-readable timestamp in UTC with nanosecond precision.
// Equivalent to AppendTime with format "2006-01-02T15:04:05.999999999Z".
func UTCNanos(buf []byte) []byte {
	return AppendTimeNanosUTC(buf, time.Now())
}

// Unix timestamp with second precision.
func UnixSecs(buf []byte) []byte {
	return AppendInt(buf, time.Now().Unix())
}

// Unix timestamp with millisecond precision.
func UnixMillis(buf []byte) []byte {
	return AppendInt(buf, time.Now().UnixMilli())
}

// Unix timestamp with microsecond precision.
func UnixMicros(buf []byte) []byte {
	return AppendInt(buf, time.Now().UnixMicro())
}

// Unix timestamp with nanosecond precision.
func UnixNanos(buf []byte) []byte {
	return AppendInt(buf, time.Now().UnixNano())
}
