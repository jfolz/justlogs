package justlogs

import (
	"fmt"
	"io"
	"runtime"
)

type Logger struct {
	// Logs are output here.
	w io.Writer
	// Minimum severity level that lines need to be output.
	Level Level
	// If the logger is disabled all operations turn to noop,
	// including adding fields to `With()` contexts.
	// It is recommended to set this value once per logger and leave it.
	Disabled bool
	// Static context fields get stored here.
	Context []byte
	// How many stack frames to skip for the caller field.
	// Values <=0 disable the caller field.
	// 1 adds the line that called the logging function.
	// 2 or more skip further stack frames.
	Caller int
	// What the caller field is called.
	CallerField string
	// What the error field is calles.
	ErrorField string
	// What the message fiels is called.
	MessageField string
	// What the timestamp field is called.
	TimestampField string
	// The function that adds the timestamp.
	TimestampFunc func([]byte) []byte
	// Dummy line returned when nothing is to be output.
	noopLine *NoopLine
}

func NewLogger(w io.Writer, level Level) *Logger {
	l := &Logger{
		w:              w,
		Level:          level,
		CallerField:    "caller",
		ErrorField:     "error",
		MessageField:   "msg",
		TimestampField: "time",
	}
	l.noopLine = NewNoopLine(l)
	return l
}

// Start a new context that can hold static fields.
// Useful if you want to produce a bunch of lines
// with identical fields and values.
//
// Note: This is a noop if the logger is disabled,
// so no fields are actually added.
func (l *Logger) With() Line {
	if l.Disabled {
		return l.noopLine
	}
	return GetRealLine(l, io.Discard, false)
}

// Check if the given level would produce a log message.
// Only useful for hot code and low severity levels (trace, debug),
// or if creating field values in your code takes a lot of time.
func (l *Logger) Enabled(level Level) bool {
	return !l.Disabled && level.Severity >= l.Level.Severity
}

func (l *Logger) log(level Level, msg string, values ...interface{}) Line {
	// return noop if severity too low
	if l.Disabled || !l.Enabled(level) {
		return l.noopLine
	}

	// get line from the pool
	line := GetRealLine(l, l.w, level.Severity >= Fatal.Severity)

	// add date or timestamp
	if l.TimestampFunc != nil {
		line.Buf = append(l.TimestampFunc(AppendKey(line.Buf, l.TimestampField)), ' ')
	}

	// add level
	line.Buf = AppendBytes(line.Buf, level.Repr)

	// add caller
	if l.Caller > 0 {
		if _, file, lno, ok := runtime.Caller(l.Caller + 1); ok {
			line.Buf = AppendInt(append(AppendFilenameBase(AppendSpaceKey(
				line.Buf, l.CallerField), file), ':'), int64(lno))
		}
	}

	// add message & context to the line buffer
	if len(values) > 0 {
		msg = fmt.Sprintf(msg, values...)
	}
	line.Buf = AppendBytes(AppendStringSafe(AppendSpaceKey(
		line.Buf, l.MessageField), msg), l.Context)

	return line
}

// Start a new line with the given level and message.
func (l *Logger) Log(level Level, msg string, values ...interface{}) Line {
	return l.log(level, msg, values...)
}

// Start a new line with level trace and the given message.
func (l *Logger) Trace(msg string, values ...interface{}) Line {
	return l.log(Trace, msg, values...)
}

// Start a new line with level debug and the given message.
func (l *Logger) Debug(msg string, values ...interface{}) Line {
	return l.log(Debug, msg, values...)
}

// Start a new line with level info and the given message.
func (l *Logger) Info(msg string, values ...interface{}) Line {
	return l.log(Info, msg, values...)
}

// Start a new line with level warn and the given message.
func (l *Logger) Warn(msg string, values ...interface{}) Line {
	return l.log(Warn, msg, values...)
}

// Start a new line with level error and the given message.
func (l *Logger) Error(msg string, values ...interface{}) Line {
	return l.log(Error, msg, values...)
}

// Start a new line with level fatal and the given message.
func (l *Logger) Fatal(msg string, values ...interface{}) Line {
	return l.log(Fatal, msg, values...)
}
