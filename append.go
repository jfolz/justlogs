package justlogs

import (
	"os"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

var (
	// Hexadecimal bytes
	hexchars = [...]byte{
		'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
	}
)

// Append a Boolean value to the given buffer and return the result.
func AppendBool(buf []byte, val bool) []byte {
	return strconv.AppendBool(buf, val)
}

// Append some raw bytes to the given buffer and return the result.
// Note that there are not safety checks here.
// Whatever you put in is appended verbatim.
func AppendBytes(buf []byte, b []byte) []byte {
	return append(buf, b...)
}

// Append a complex64 value to the given buffer and return the result.
func AppendComplex64(buf []byte, val complex64) []byte {
	im := imag(val)
	// avoid -0.0
	if im == 0 {
		im *= im
	}
	buf = AppendFloat32(append(buf, '('), real(val))
	if im >= 0 {
		buf = append(buf, '+')
	}
	return append(AppendFloat32(buf, im), 'i', ')')
}

// Append a complex128 value to the given buffer and return the result.
func AppendComplex128(buf []byte, val complex128) []byte {
	im := imag(val)
	if im == 0 {
		im *= im
	}
	buf = AppendFloat64(append(buf, '('), real(val))
	if im >= 0 {
		buf = append(buf, '+')
	}
	return append(AppendFloat64(buf, im), 'i', ')')
}

// Append a duration to the given buffer and return the result.
func AppendDuration(buf []byte, val time.Duration) []byte {
	return AppendStringUnsafe(buf, val.String())
}

func AppendFilenameBase(buf []byte, path string) []byte {
	i := len(path) - 1
	for i >= 0 && !os.IsPathSeparator(path[i]) {
		i--
	}
	return AppendStringUnsafe(buf, path[i+1:])
}

// Append a float32 value to the given buffer and return the result.
func AppendFloat32(buf []byte, val float32) []byte {
	return strconv.AppendFloat(buf, float64(val), 'f', -1, 32)
}

// Append a float64 value to the given buffer and return the result.
func AppendFloat64(buf []byte, val float64) []byte {
	return strconv.AppendFloat(buf, val, 'f', -1, 64)
}

// Append a the hex representation of some bytes to the given buffer and return the result.
func AppendHex(buf []byte, b []byte) []byte {
	buf = append(buf, '0', 'x')
	for _, v := range b {
		buf = append(buf, hexchars[v>>4], hexchars[v&0x0f])
	}
	return buf
}

// Append an int64 value to the given buffer and return the result.
func AppendInt(buf []byte, val int64) []byte {
	return strconv.AppendInt(buf, val, 10)
}

// Append a "<key>=" to the given buffer and return the result.
func AppendKey(buf []byte, key string) []byte {
	return append(AppendStringUnsafe(buf, key), '=')
}

/*
Returns true if this rune needs to be enclosed in quotes.

RuneNeedsQuote is adapted from the logfmt package:
https://github.com/go-logfmt/logfmt/blob/master/encode.go

LICENSE
The MIT License (MIT)

Copyright (c) 2015 go-logfmt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
func RuneNeedsQuote(r rune) bool {
	return r <= ' ' || r == '=' || r == '"' || r == utf8.RuneError
}

/*
Append an escaped and quoted string
to the given buffer and return the result.

AppendQuotedString is adapted from Go's JSON encoder:
https://github.com/golang/go/blob/master/src/encoding/json/encode.go

LICENSE
Copyright (c) 2009 The Go Authors. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
   * Neither the name of Google Inc. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
func AppendQuotedString(buf []byte, s string) []byte {
	buf = append(buf, '"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if 0x20 <= b && b != '\\' && b != '"' {
				i++
				continue
			}
			if start < i {
				buf = append(buf, s[start:i]...)
			}
			switch b {
			case '\\', '"':
				buf = append(buf, '\\', b)
			case '\n':
				buf = append(buf, '\\', 'n')
			case '\r':
				buf = append(buf, '\\', 'r')
			case '\t':
				buf = append(buf, '\\', 't')
			default:
				// This encodes bytes < 0x20 except for \n, \r, and \t.
				buf = append(buf, `\u00`...)
				buf = append(buf, hexchars[b>>4], hexchars[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError {
			if start < i {
				buf = append(buf, s[start:i]...)
			}
			buf = append(buf, `\ufffd`...)
			i += size
			start = i
			continue
		}
		// JSONP things, see go.dev/src/encoding/json/encode.go
		if c == '\u2028' || c == '\u2029' {
			if start < i {
				buf = append(buf, s[start:i]...)
			}
			buf = append(buf, `\u202`...)
			buf = append(buf, hexchars[c&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		buf = append(buf, s[start:]...)
	}
	return append(buf, '"')
}

// Line AppendKey, but also appends a whitespace before the key.
func AppendSpaceKey(buf []byte, key string) []byte {
	return AppendKey(append(buf, ' '), key)
}

// Append a string (quoted and escaped if necessary)
// to the given buffer and return the result.
func AppendStringSafe(buf []byte, val string) []byte {
	if val == "null" {
		return AppendStringUnsafe(buf, `"null"`)
	} else if strings.IndexFunc(val, RuneNeedsQuote) != -1 {
		return AppendQuotedString(buf, val)
	} else {
		return AppendStringUnsafe(buf, val)
	}
}

// Append a string (without checking whether it needs quotes and escapes)
// to the given buffer and return the result.
func AppendStringUnsafe(buf []byte, s string) []byte {
	return append(buf, s...)
}

// Append a uint64 value to the given buffer and return the result.
func AppendUint(buf []byte, val uint64) []byte {
	return strconv.AppendUint(buf, val, 10)
}
