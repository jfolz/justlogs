Simple structured logging for humans in Go.

Heavily inspired by [zerolog](https://github.com/rs/zerolog),
but instead of JSON it only ever produces nicely readable logfmt
with fields in places that make sense.

Lines follow this format:
```
[time=date/timestamp] level=thelevel [caller=file.go:lno] msg="your message" [optional fields]
```



# Performance

I was looking for a new logging library that produces logfmt output
and accidentally made a really fast and basically zero allocations
logging library instead.

Here's numbers for the zap benchmark on AMD EPYC 7742 with `GOMAXPROCS=2`.

Log a message and 10 fields:

| Package   | Time ns/op | Time % to justlogs | Bytes/op | Allocs/op |
|-----------|-----------:|-------------------:|---------:|----------:|
| justlogs  |       1632 |                +0% |        0 |         0 |
| :zap: zap |       2733 |               +67% |      736 |         5 |
| zerolog   |      10639 |              +552% |     2568 |        32 |
| go-kit    |      11736 |              +619% |     3430 |        59 |
| logrus    |      14689 |              +800% |     6332 |        81 |
| apex/log  |      26427 |             +1519% |     4590 |        66 |
| log15     |      28011 |             +1616% |     6792 |        76 |

Log a message with a logger that already has 10 fields of context:

| Package   | Time ns/op | Time % to justlogs | Bytes/op | Allocs/op |
|-----------|-----------:|-------------------:|---------:|----------:|
| justlogs  |      136.9 |                +0% |        0 |         0 |
| zerolog   |      261.5 |               +91% |        0 |         0 |
| :zap: zap |      329.5 |              +140% |        0 |         0 |
| go-kit    |    12079.0 |             +8723% |     3787 |        58 |
| logrus    |    13543.0 |             +9793% |     4822 |        70 |
| log15     |    19427.0 |            +14090% |     3361 |        72 |
| apex/log  |    25105.0 |            +18238% |     3416 |        55 |

Log a static string, without any context or `printf`-style templating:

| Package        | Time ns/op | Time % to justlogs | Bytes/op | Allocs/op |
|----------------|-----------:|-------------------:|---------:|----------:|
| justlogs       |      129.9 |                +0% |        0 |         0 |
| zerolog        |      288.7 |              +122% |        0 |         0 |
| stdlib.Println |      330.7 |              +155% |       80 |         2 |
| :zap: zap      |      497.9 |              +283% |        0 |         0 |
| go-kit         |      632.2 |              +387% |      648 |        11 |
| logrus         |     1592.0 |             +1125% |     1264 |        25 |
| apex/log       |     1897.0 |             +1360% |      369 |         7 |
| log15          |     3798.0 |             +2824% |     1790 |        22 |



# Basic usage

With justlogs you build up the log line as you go.
The `Logger.Log`, `Logger.Trace`, `Logger.Debug`, `Logger.Info`,
`Logger.Warn`, `Logger.Error`, and `Logger.Fatal` methods
return a new `Line` with the given message.
You can then add typed fields to the line.
Finally call `Line.End` to output the finished line.

```Go
package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// just a message
	log.Debug("just some text").End()
	// add fields to your logs
	log.Info("hello logs").
		S("greeting", "yes").
		B("success", true).
		I("number", 42).
		H("hex", []byte{1, 2, 3, 4}).
		End()
}
```

Result:
```
$ go run examples/simple.go 
level=debug message="just some text"
level=info message="hello logs" greeting=yes success=true number=42 hex=0x01020304
```



# Timestamp & caller

Timestamp and caller are off by default.
Set the `Logger.TimestampFunc` to one of the provided
`Timezone*` or `UTC*` functions to enable timestamps.
You can use your own `func (buf []byte) []byte` function that
appends the timestamp to the given buffer and returns the result.

Setting `Logger.Caller` to `1` adds caller filename and line number
to the line.
Values greater `1` skip additional stack frames.
If you have a function that wraps calls to the logger,
you can set it to `2` to output the caller of your wrapper instead.

```Go
package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// use unix timestamp
	log.TimestampFunc = jl.UnixSecs
	log.Debug("with unix timestamp").End()
	// turn on human-readable timestamp
	log.TimestampFunc = jl.UTCMicros
	log.Debug("with human-readable timestamp").End()
	// turn on caller
	log.Caller = 1
	log.Debug("now with caller").End()
}
```

Result:
```
$ go run examples/timestamp_caller.go 
time=1638546476 level=debug message="with unix timestamp"
time=2021-12-03T15:47:56.430105Z level=debug message="with human-readable timestamp"
time=2021-12-03T15:47:56.430134Z level=debug caller=timestamp_caller.go:21 message="now with caller"
```



# Static context

Contexts are useful if you want to produce a bunch of lines
with identical fields and values.
Start a new context with the `Logger.With` method.
It returns a dummy line that you can add fields to.
Then call `Line.Ctx` (instead of `Line.End`) to create a new logger
with the added context.
You can do this as many times as you like to create sub-loggers.

```Go
package main

import (
	"errors"
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	base := jl.NewLogger(os.Stdout, jl.Debug)
	// make a sub-logger with some static context fields
	log := base.With().E(errors.New("something bad happened")).Ctx()
	// use the sub-logger
	log.Error("oh no!").End()
	log.Fatal("there is nothing we can do now!").End()
}
```

Result:
```
$ go run examples/context.go 
level=error message="oh no!" error="something bad happened"
level=fatal message="there is nothing we can do now!" error="something bad happened"
exit status 1
```



# Arrays

Use arrays if you want to add multiple comma-separated values to one field,
e.g. a slice of integers.
Start a new array with `Line.A`.
It returns an `Array` that you can add values to.
Call `Array.End` to finish the array and return to the log line.
You can now add more fields or end the line.

You can mix and match different types of values (ints, strings, ...),
though I don't know what good that would do.

```Go
package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// start a new array
	array := log.Info("logging some arrays").A("myarray")
	// add some ints to it
	for _, i := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {
		array.I(i)
	}
	// and a string for some reason
	array.S("I am also a number")
	// finish the array and output the line
	array.End().F32("notarray", 0.1).End()
}
```

Result:
```
$ go run examples/array.go 
level=info message="logging some arrays" myarray=1,2,3,4,5,6,7,8,9,10,"I am also a number" notarray=0.1
```



# Formatting

All basic logging functions like `Logger.Info`, `Line.S`, and `Array.S`
can perform string formatting.
They accept optional variadic arguments that if given instruct the function
to apply `fmt.Sprintf(string, args...)` to the given string.
While this can be convenient it is also rather slow.

```Go
package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// a formatted message
	log.Debug("%s %d %v", "text", 234, []byte{1, 2, 3, 4}).End()
	// a formatted string
	log.Info("formatted strings").S("formatted", "%f %s", 1.213, "string").End()
	// formatted string in an array
	log.Info("formatted strings").A("formatarray").
		S("%s:%f", "a", 1.5).S("%s:%f", "b", 9.32).End().End()
}
```

Result:
```
$ go run examples/formatting.go 
level=debug msg="text 234 [1 2 3 4]"
level=info msg="formatted strings" formatted="1.213000 string"
level=info msg="formatted strings" formatarray=a:1.500000,b:9.320000
```



# Caveats

With justlogs you build up the log line as you go.
Every field you add is simply added to some buffer until your line is
ready to be output.
There are no checks if your optional fields contain duplicates or even
"overwrite" a standard field as that would slow things down significantly.
It's your job to make sure your logs make sense.

```Go
package main

import (
	"os"

	jl "gitlab.com/jfolz/justlogs"
)

func main() {
	// create a logger with an output stream and level
	log := jl.NewLogger(os.Stdout, jl.Debug)
	// duplicate field name
	log.Info("the message").I("number", 1).I("number", 2).I("number", 3).End()
	// use a "reserved" field name
	log.Info("the message").S("msg", "more message?").End()
}
```

Result:
```
$ go run examples/caveat.go 
level=info msg="the message" number=1 number=2 number=3
level=info msg="the message" msg="more message?"
```